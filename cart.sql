-- cart.customer_ids definition

-- Drop table

-- DROP TABLE cart.customer_ids;

CREATE TABLE cart.customer_ids (
	core varchar NOT NULL,
	bc_ship_to varchar NULL,
	sap_ship_to varchar NOT NULL,
	sap_sold_to varchar NOT NULL,
	bc_sold_to varchar NOT NULL,
	CONSTRAINT customer_ids_pkey PRIMARY KEY (core)
);


-- cart.product_ids definition

-- Drop table

-- DROP TABLE cart.product_ids;

CREATE TABLE cart.product_ids (
	core varchar NOT NULL,
	sap varchar NULL,
	big_commerce varchar NULL,
	CONSTRAINT product_ids_pkey PRIMARY KEY (core)
);