
-- Drop table

-- DROP TABLE bc.customer_id;

CREATE TABLE bc.customer_id (
	bc_id varchar(1000) NULL,
	core_id varchar(1000) NULL,
	sap_id varchar(1000) NULL
);

-- Drop table

-- DROP TABLE bc."order";

CREATE TABLE bc."order" (
	base_handling_cost varchar NULL,
	base_shipping_cost varchar NULL,
	base_wrapping_cost varchar NULL,
	cart_id varchar NULL,
	channel_id int4 NULL,
	coupon_discount varchar NULL,
	credit_card_type numeric(18,6) NULL,
	currency_code varchar NULL,
	currency_exchange_rate varchar NULL,
	currency_id int4 NULL,
	custom_status varchar NULL,
	customer_id int4 NULL,
	customer_message varchar NULL,
	date_created varchar NULL,
	date_modified varchar NULL,
	date_shipped varchar NULL,
	default_currency_code varchar NULL,
	default_currency_id int4 NULL,
	discount_amount varchar NULL,
	ebay_order_id varchar NULL,
	external_id varchar NULL,
	external_merchant_id int4 NULL,
	external_source varchar NULL,
	geoip_country varchar NULL,
	geoip_country_iso2 varchar NULL,
	gift_certificate_amount varchar NULL,
	handling_cost_ex_tax varchar NULL,
	handling_cost_inc_tax varchar NULL,
	handling_cost_tax varchar NULL,
	handling_cost_tax_class_id int4 NULL,
	id int4 NULL,
	ip_address varchar NULL,
	is_deleted bool NULL,
	is_email_opt_in bool NULL,
	items_shipped numeric(18,6) NULL,
	items_total numeric(18,6) NULL,
	order_is_digital bool NULL,
	order_source varchar NULL,
	payment_method varchar NULL,
	payment_provider_id varchar NULL,
	payment_status varchar NULL,
	refunded_amount varchar NULL,
	shipping_address_count int4 NULL,
	shipping_cost_ex_tax varchar NULL,
	shipping_cost_inc_tax varchar NULL,
	shipping_cost_tax varchar NULL,
	shipping_cost_tax_class_id int4 NULL,
	staff_notes varchar NULL,
	status varchar NULL,
	status_id int4 NULL,
	store_credit_amount varchar NULL,
	store_default_currency_code varchar NULL,
	store_default_to_transactional_exchange_rate varchar NULL,
	subtotal_ex_tax varchar NULL,
	subtotal_inc_tax varchar NULL,
	subtotal_tax varchar NULL,
	tax_provider_id varchar NULL,
	total_ex_tax varchar NULL,
	total_inc_tax varchar NULL,
	total_tax varchar NULL,
	wrapping_cost_ex_tax varchar NULL,
	wrapping_cost_inc_tax varchar NULL,
	wrapping_cost_tax varchar NULL,
	wrapping_cost_tax_class_id int4 NULL
);

-- Drop table

-- DROP TABLE bc.order_line;

CREATE TABLE bc.order_line (
	base_cost_price varchar NULL,
	base_price varchar NULL,
	base_total varchar NULL,
	base_wrapping_cost varchar NULL,
	bin_picking_number varchar NULL,
	cost_price_ex_tax varchar NULL,
	cost_price_inc_tax varchar NULL,
	cost_price_tax varchar NULL,
	"depth" varchar NULL,
	ebay_item_id varchar NULL,
	ebay_transaction_id varchar NULL,
	event_date varchar NULL,
	event_name varchar NULL,
	external_id int4 NULL,
	fixed_shipping_cost varchar NULL,
	fulfillment_source varchar NULL,
	height varchar NULL,
	id int4 NULL,
	is_bundled_product bool NULL,
	is_refunded bool NULL,
	"name" varchar NULL,
	option_set_id int4 NULL,
	order_address_id int4 NULL,
	order_id int4 NULL,
	parent_order_product_id int4 NULL,
	price_ex_tax varchar NULL,
	price_inc_tax varchar NULL,
	price_tax varchar NULL,
	product_id int4 NULL,
	quantity numeric(18,6) NULL,
	quantity_refunded numeric(18,6) NULL,
	quantity_shipped numeric(18,6) NULL,
	refund_amount varchar NULL,
	return_id int4 NULL,
	sku varchar NULL,
	total_ex_tax varchar NULL,
	total_inc_tax varchar NULL,
	total_tax varchar NULL,
	"type" varchar NULL,
	upc varchar NULL,
	variant_id int4 NULL,
	weight numeric(18,6) NULL,
	width varchar NULL,
	wrapping_cost_ex_tax varchar NULL,
	wrapping_cost_inc_tax varchar NULL,
	wrapping_cost_tax varchar NULL,
	wrapping_message varchar NULL,
	wrapping_name varchar NULL
);

-- Drop table

-- DROP TABLE bc.order_shipping_address;

CREATE TABLE bc.order_shipping_address (
	base_cost varchar NULL,
	base_handling_cost varchar NULL,
	city varchar NULL,
	company varchar NULL,
	cost_ex_tax varchar NULL,
	cost_inc_tax varchar NULL,
	cost_tax varchar NULL,
	cost_tax_class_id int4 NULL,
	country varchar NULL,
	country_iso2 varchar NULL,
	email varchar NULL,
	first_name varchar NULL,
	handling_cost_ex_tax varchar NULL,
	handling_cost_inc_tax varchar NULL,
	handling_cost_tax varchar NULL,
	handling_cost_tax_class_id int4 NULL,
	id int4 NULL,
	items_shipped numeric(18,6) NULL,
	items_total numeric(18,6) NULL,
	last_name varchar NULL,
	order_id int4 NULL,
	phone varchar NULL,
	shipping_method varchar NULL,
	shipping_zone_id numeric(18,6) NULL,
	shipping_zone_name varchar NULL,
	state varchar NULL,
	street_1 varchar NULL,
	street_2 varchar NULL,
	zip varchar NULL
);

-- Drop table

-- DROP TABLE bc.product_id;

CREATE TABLE bc.product_id (
	bc_id varchar(1000) NULL,
	core_id varchar(1000) NULL,
	sap_id varchar(1000) NULL
);

