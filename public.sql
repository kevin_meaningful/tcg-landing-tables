-- public.bc_order definition

-- Drop table

-- DROP TABLE public.bc_order;

CREATE TABLE public.bc_order (
	date_shipped varchar(1000) NULL,
	billing_address_company varchar(1000) NULL,
	billing_address_street_1 varchar(1000) NULL,
	discount_amount varchar(1000) NULL,
	handling_cost_ex_tax varchar(1000) NULL,
	billing_address_country_iso2 varchar(1000) NULL,
	billing_address_street_2 varchar(1000) NULL,
	handling_cost_inc_tax varchar(1000) NULL,
	billing_address_zip varchar(1000) NULL,
	items_total int8 NULL,
	handling_cost_tax varchar(1000) NULL,
	status_id int8 NULL,
	total_ex_tax varchar(1000) NULL,
	base_handling_cost varchar(1000) NULL,
	id int8 NULL,
	billing_address_email varchar(1000) NULL,
	payment_method varchar(1000) NULL,
	default_currency_id int8 NULL,
	billing_address_city varchar(1000) NULL,
	customer_message varchar(1000) NULL,
	base_shipping_cost varchar(1000) NULL,
	total_tax varchar(1000) NULL,
	currency_exchange_rate varchar(1000) NULL,
	refunded_amount varchar(1000) NULL,
	order_is_digital bool NULL,
	channel_id int8 NULL,
	items_shipped int8 NULL,
	subtotal_ex_tax varchar(1000) NULL,
	status varchar(1000) NULL,
	total_inc_tax varchar(1000) NULL,
	external_id varchar(1000) NULL,
	handling_cost_tax_class_id int8 NULL,
	currency_code varchar(1000) NULL,
	subtotal_inc_tax varchar(1000) NULL,
	is_deleted bool NULL,
	shipping_cost_tax_class_id int8 NULL,
	billing_address_country varchar(1000) NULL,
	billing_address_state varchar(1000) NULL,
	external_source varchar(1000) NULL,
	staff_notes varchar(1000) NULL,
	default_currency_code varchar(1000) NULL,
	date_created varchar(1000) NULL,
	billing_address_first_name varchar(1000) NULL,
	date_modified varchar(1000) NULL,
	billing_address_phone varchar(1000) NULL,
	order_source varchar(1000) NULL,
	customer_id int8 NULL,
	subtotal_tax varchar(1000) NULL,
	currency_id int8 NULL,
	billing_address_last_name varchar(1000) NULL
);


-- public.chartofaccounts definition

-- Drop table

-- DROP TABLE public.chartofaccounts;

CREATE TABLE public.chartofaccounts (
	chartofaccountsname varchar NULL,
	chartofaccounts varchar NULL
);


-- public.chartofaccountsuuid definition

-- Drop table

-- DROP TABLE public.chartofaccountsuuid;

CREATE TABLE public.chartofaccountsuuid (
	chartofaccounts varchar NOT NULL,
	uuid varchar(36) NOT NULL,
	CONSTRAINT chartofaccountsuuid_pkey PRIMARY KEY (chartofaccounts)
);


-- public.company definition

-- Drop table

-- DROP TABLE public.company;

CREATE TABLE public.company (
	companycode int4 NOT NULL,
	companycodename varchar NOT NULL,
	country varchar NULL,
	city_name varchar NULL,
	CONSTRAINT company_pkey PRIMARY KEY (companycode)
);


-- public.companyuuid definition

-- Drop table

-- DROP TABLE public.companyuuid;

CREATE TABLE public.companyuuid (
	companycode int4 NOT NULL,
	uuid varchar(36) NOT NULL,
	CONSTRAINT companyuuid_pkey PRIMARY KEY (companycode)
);


-- public.costcentre definition

-- Drop table

-- DROP TABLE public.costcentre;

CREATE TABLE public.costcentre (
	costcentre int4 NOT NULL,
	costcentredescription varchar NOT NULL,
	profitcentre int4 NOT NULL,
	validity_end_date timestamptz NOT NULL,
	validity_start_date timestamptz NULL,
	CONSTRAINT costcentre_pkey PRIMARY KEY (costcentre, validity_end_date)
);


-- public.costcentrepc definition

-- Drop table

-- DROP TABLE public.costcentrepc;

CREATE TABLE public.costcentrepc (
	profitcentrecode int4 NOT NULL,
	divisionname varchar NOT NULL,
	sitename varchar NOT NULL,
	CONSTRAINT costcentrepc_pkey PRIMARY KEY (profitcentrecode)
);


-- public.costcentreuuid definition

-- Drop table

-- DROP TABLE public.costcentreuuid;

CREATE TABLE public.costcentreuuid (
	costcentre int4 NOT NULL,
	uuid varchar(36) NOT NULL,
	CONSTRAINT costcentreuuid_pkey PRIMARY KEY (costcentre)
);


-- public.currencyuuid definition

-- Drop table

-- DROP TABLE public.currencyuuid;

CREATE TABLE public.currencyuuid (
	currencycode varchar NOT NULL,
	uuid varchar(36) NOT NULL,
	CONSTRAINT currencyuuid_pkey PRIMARY KEY (currencycode)
);


-- public.dateinstantuuid definition

-- Drop table

-- DROP TABLE public.dateinstantuuid;

CREATE TABLE public.dateinstantuuid (
	datecode int4 NOT NULL,
	uuid varchar(36) NOT NULL,
	CONSTRAINT dateinstantuuid_pkey PRIMARY KEY (datecode)
);


-- public.employeeuuid definition

-- Drop table

-- DROP TABLE public.employeeuuid;

CREATE TABLE public.employeeuuid (
	accountingdoccreatedbyuser varchar NOT NULL,
	uuid varchar(36) NOT NULL,
	CONSTRAINT employeeuuid_pkey PRIMARY KEY (accountingdoccreatedbyuser)
);


-- public.functionalarea definition

-- Drop table

-- DROP TABLE public.functionalarea;

CREATE TABLE public.functionalarea (
	functionalareaname varchar NOT NULL,
	functionalarea varchar NOT NULL,
	CONSTRAINT functionalarea_pkey PRIMARY KEY (functionalarea)
);


-- public.functionalareauuid definition

-- Drop table

-- DROP TABLE public.functionalareauuid;

CREATE TABLE public.functionalareauuid (
	uuid varchar(36) NOT NULL,
	functionalarea varchar NOT NULL,
	CONSTRAINT functionalareauuid_pkey PRIMARY KEY (functionalarea)
);


-- public.glaccount definition

-- Drop table

-- DROP TABLE public.glaccount;

CREATE TABLE public.glaccount (
	glaccount varchar NOT NULL,
	glaccountname varchar NOT NULL,
	chartofaccounts varchar NOT NULL,
	isbalancesheetaccount bool NOT NULL,
	glaccounttype varchar(1) NOT NULL,
	isprofitlossaccount bool NOT NULL,
	CONSTRAINT glaccount_pkey PRIMARY KEY (glaccount)
);


-- public.glaccountuuid definition

-- Drop table

-- DROP TABLE public.glaccountuuid;

CREATE TABLE public.glaccountuuid (
	glaccount varchar NOT NULL,
	uuid varchar(36) NULL,
	CONSTRAINT glaccountuuid_pkey PRIMARY KEY (glaccount)
);


-- public.gljournal definition

-- Drop table

-- DROP TABLE public.gljournal;

CREATE TABLE public.gljournal (
	id varchar(36) NOT NULL,
	lastchangedatetime varchar NULL,
	accountingdocumenttypename varchar NULL,
	accountingdocument varchar NULL,
	sourceledger varchar NOT NULL,
	ledger varchar NOT NULL,
	fiscalyear int4 NOT NULL,
	companycode int4 NOT NULL,
	gllineitemcount int4 NOT NULL,
	accountingdocumenttype varchar NULL
);


-- public.gljournalentryitem definition

-- Drop table

-- DROP TABLE public.gljournalentryitem;

CREATE TABLE public.gljournalentryitem (
	amountincompanycodecurrency varchar NOT NULL,
	amountinglobalcurrency varchar NOT NULL,
	amountintransactioncurrency varchar NOT NULL,
	ledgergllineitem varchar NOT NULL,
	postingdate int4 NOT NULL,
	companycodecurrency varchar NOT NULL,
	globalcurrency varchar NOT NULL,
	transactioncurrency varchar NOT NULL,
	documentdate int4 NOT NULL,
	accountingdoccreatedbyuser varchar NOT NULL,
	profitcentre int4 NOT NULL,
	costcentre int4 NULL,
	functionalarea varchar NULL,
	accountingdocument varchar NOT NULL,
	glaccount varchar NOT NULL,
	companycode int4 NOT NULL,
	ledger varchar NOT NULL,
	id varchar(36) NOT NULL,
	lastchangedatetime timestamptz NOT NULL,
	accountingdocumenttypename varchar NOT NULL,
	sourceid varchar NOT NULL,
	sourceledger varchar NOT NULL,
	fiscalyear int4 NOT NULL,
	fiscalperiod varchar NOT NULL,
	documentitemtext varchar NULL,
	accountingdocumenttype varchar NULL,
	assignmentreference varchar NULL,
	salesdocument varchar NULL,
	salesdocumentitem varchar NULL
);


-- public.gljournalentryitemsalesdocument definition

-- Drop table

-- DROP TABLE public.gljournalentryitemsalesdocument;

CREATE TABLE public.gljournalentryitemsalesdocument (
	id varchar NOT NULL,
	salesdocument varchar NULL,
	salesdocumentitem varchar NULL,
	CONSTRAINT gljournalentryitemsalesdocument_pkey PRIMARY KEY (id)
);


-- public.gllineitem definition

-- Drop table

-- DROP TABLE public.gllineitem;

CREATE TABLE public.gllineitem (
	amountincompanycodecurrency varchar NOT NULL,
	amountinglobalcurrency varchar NOT NULL,
	amountintransactioncurrency varchar NOT NULL,
	ledgergllineitem varchar NOT NULL,
	postingdate varchar NOT NULL,
	companycodecurrency varchar NOT NULL,
	globalcurrency varchar NOT NULL,
	transactioncurrency varchar NOT NULL,
	documentdate varchar NOT NULL,
	accountingdoccreatedbyuser varchar NOT NULL,
	profitcentre varchar NOT NULL,
	costcentre varchar NULL,
	functionalarea varchar NOT NULL,
	accountingdocument varchar NOT NULL,
	glaccount varchar NOT NULL,
	companycode varchar NOT NULL,
	ledger varchar NOT NULL
);


-- public.lastmodified definition

-- Drop table

-- DROP TABLE public.lastmodified;

CREATE TABLE public.lastmodified (
	entity varchar NOT NULL,
	lastmodified timestamptz NULL,
	CONSTRAINT lastmodified_pkey PRIMARY KEY (entity)
);


-- public.leadingledger definition

-- Drop table

-- DROP TABLE public.leadingledger;

CREATE TABLE public.leadingledger (
	ledgercode varchar NOT NULL
);


-- public.ledger definition

-- Drop table

-- DROP TABLE public.ledger;

CREATE TABLE public.ledger (
	ledger varchar NOT NULL,
	ledgername varchar NOT NULL,
	isleadingledger varchar NOT NULL
);


-- public.ledgeruuid definition

-- Drop table

-- DROP TABLE public.ledgeruuid;

CREATE TABLE public.ledgeruuid (
	ledger varchar NOT NULL,
	uuid varchar(36) NOT NULL,
	CONSTRAINT ledgeruuid_pkey PRIMARY KEY (ledger)
);


-- public.profitcentre definition

-- Drop table

-- DROP TABLE public.profitcentre;

CREATE TABLE public.profitcentre (
	profitcentrelongname varchar NULL,
	profitcentre int4 NOT NULL,
	addressname varchar NULL,
	profitcentrestandardhierarchy varchar NOT NULL,
	region varchar NULL,
	cityname varchar NULL,
	country varchar NULL
);


-- public.profitcentregroup definition

-- Drop table

-- DROP TABLE public.profitcentregroup;

CREATE TABLE public.profitcentregroup (
	profitcentregroup varchar(4) NOT NULL,
	profitcentregroupname varchar NULL,
	CONSTRAINT profitcentregroup_pkey PRIMARY KEY (profitcentregroup)
);


-- public.profitcentretest definition

-- Drop table

-- DROP TABLE public.profitcentretest;

CREATE TABLE public.profitcentretest (
	profitcentre uuid NULL
);


-- public.profitcentreuuid definition

-- Drop table

-- DROP TABLE public.profitcentreuuid;

CREATE TABLE public.profitcentreuuid (
	profitcentre int4 NOT NULL,
	uuid varchar(36) NOT NULL,
	CONSTRAINT profitcentreuuid_pkey PRIMARY KEY (profitcentre)
);


-- public.sourcesystemuuid definition

-- Drop table

-- DROP TABLE public.sourcesystemuuid;

CREATE TABLE public.sourcesystemuuid (
	sourcesystem varchar NOT NULL,
	uuid varchar(36) NOT NULL,
	CONSTRAINT sourcesystem_pkey PRIMARY KEY (sourcesystem)
);