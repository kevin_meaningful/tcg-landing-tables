-- pricing.customer_ids definition

-- Drop table

-- DROP TABLE pricing.customer_ids;

CREATE TABLE pricing.customer_ids (
	core varchar NOT NULL,
	sap varchar NULL,
	big_commerce varchar NULL,
	CONSTRAINT customer_ids_pkey PRIMARY KEY (core)
);


-- pricing.customer_sales_area definition

-- Drop table

-- DROP TABLE pricing.customer_sales_area;

CREATE TABLE pricing.customer_sales_area (
	customer varchar NOT NULL,
	sales_organization varchar NOT NULL,
	distribution_channel varchar NOT NULL,
	customer_group varchar NULL,
	customer_price_group varchar NULL,
	CONSTRAINT customer_sales_area_pkey PRIMARY KEY (customer, sales_organization, distribution_channel)
);


-- pricing.product_ids definition

-- Drop table

-- DROP TABLE pricing.product_ids;

CREATE TABLE pricing.product_ids (
	core varchar NOT NULL,
	sap varchar NULL,
	big_commerce varchar NULL,
	CONSTRAINT product_ids_pkey PRIMARY KEY (core)
);


-- pricing.sales_pricing_condition_record definition

-- Drop table

-- DROP TABLE pricing.sales_pricing_condition_record;

CREATE TABLE pricing.sales_pricing_condition_record (
	condition_record varchar NOT NULL,
	condition_sequential_number varchar NOT NULL,
	condition_table varchar NOT NULL,
	condition_validity_start_date timestamptz NOT NULL,
	condition_validity_end_date timestamptz NULL,
	condition_application varchar NOT NULL,
	condition_type varchar NOT NULL,
	condition_release_status varchar NULL,
	created_by_user varchar NULL,
	creation_date timestamptz NOT NULL,
	pricing_scale_type varchar NULL,
	pricing_scale_basis varchar NULL,
	condition_scale_quantity varchar NULL,
	condition_scale_quantity_unit varchar NULL,
	condition_scale_amount varchar NULL,
	condition_scale_amount_currency varchar NULL,
	condition_calculation_type varchar NULL,
	condition_rate_value numeric(16,3) NULL,
	condition_rate_value_unit varchar NULL,
	condition_quantity varchar NULL,
	condition_quantity_unit varchar NULL,
	condition_is_deleted bool NULL
);


-- pricing.sales_pricing_condition_record_scale definition

-- Drop table

-- DROP TABLE pricing.sales_pricing_condition_record_scale;

CREATE TABLE pricing.sales_pricing_condition_record_scale (
	condition_record varchar NOT NULL,
	condition_sequential_number varchar NOT NULL,
	condition_scale_line varchar NOT NULL,
	condition_scale_quantity varchar NULL,
	condition_scale_quantity_unit varchar NULL,
	condition_scale_amount numeric(16,3) NULL,
	condition_scale_amount_currency varchar NULL,
	condition_rate_value numeric(16,3) NULL,
	condition_rate_value_unit varchar NULL
);


-- pricing.sales_pricing_condition_record_validity definition

-- Drop table

-- DROP TABLE pricing.sales_pricing_condition_record_validity;

CREATE TABLE pricing.sales_pricing_condition_record_validity (
	condition_record varchar NOT NULL,
	condition_validity_start_date timestamptz NOT NULL,
	condition_validity_end_date timestamptz NULL,
	condition_application varchar NOT NULL,
	condition_type varchar NOT NULL,
	condition_release_status varchar NULL,
	sales_organization varchar NULL,
	division varchar NULL,
	distribution_channel varchar NULL,
	material varchar NULL,
	material_group varchar NULL,
	material_pricing_group varchar NULL,
	customer varchar NULL,
	customer_group varchar NULL,
	customer_price_group varchar NULL
);