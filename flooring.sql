-- flooring.address_email_address definition

-- Drop table

-- DROP TABLE flooring.address_email_address;

CREATE TABLE flooring.address_email_address (
	address_id varchar NOT NULL,
	email_address varchar NOT NULL
);


-- flooring.address_phone_number definition

-- Drop table

-- DROP TABLE flooring.address_phone_number;

CREATE TABLE flooring.address_phone_number (
	address_id varchar NOT NULL,
	phone_number varchar NULL,
	international_phone_number varchar NOT NULL
);


-- flooring.billing_document definition

-- Drop table

-- DROP TABLE flooring.billing_document;

CREATE TABLE flooring.billing_document (
	billing_document varchar NOT NULL,
	creation_date timestamptz NULL,
	creation_time varchar NULL,
	last_change_date_time timestamptz NULL,
	sold_to_party varchar NULL,
	total_net_amount varchar NULL,
	transaction_currency varchar NULL,
	payer_party varchar NULL,
	billing_document_date timestamptz NULL,
	exchange_rate_date timestamptz NULL,
	company_code varchar NULL,
	abslt_accounting_exchange_rate varchar NULL,
	billing_document_type varchar NULL,
	distribution_channel varchar NULL,
	billing_document_is_cancelled bool NOT NULL,
	cancelled_billing_document varchar NULL,
	CONSTRAINT billing_document_pkey PRIMARY KEY (billing_document)
);


-- flooring.billing_document_item definition

-- Drop table

-- DROP TABLE flooring.billing_document_item;

CREATE TABLE flooring.billing_document_item (
	billing_document varchar NOT NULL,
	billing_document_item varchar NOT NULL,
	material varchar NULL,
	sales_document varchar NULL,
	sales_document_item varchar NULL,
	net_amount numeric(16, 3) NULL,
	billing_quantity varchar NULL,
	billing_quantity_unit varchar NULL,
	transaction_currency varchar NULL,
	plant varchar NULL,
	created_by_user varchar NULL,
	sales_document_item_category varchar NULL,
	CONSTRAINT billing_document_item_pkey PRIMARY KEY (billing_document, billing_document_item)
);


-- flooring.billing_document_item_prcg_elmnt definition

-- Drop table

-- DROP TABLE flooring.billing_document_item_prcg_elmnt;

CREATE TABLE flooring.billing_document_item_prcg_elmnt (
	billing_document varchar NOT NULL,
	billing_document_item varchar NOT NULL,
	pricing_procedure_step varchar NOT NULL,
	pricing_procedure_counter varchar NOT NULL,
	condition_type varchar NULL,
	condition_amount numeric(16, 3) NULL,
	condition_class varchar NULL,
	condition_inactive_reason varchar NULL,
	condition_rate_value numeric(24, 9) NULL,
	CONSTRAINT billing_document_item_prcg_elmnt_pkey PRIMARY KEY (billing_document, billing_document_item, pricing_procedure_step, pricing_procedure_counter)
);


-- flooring.billing_document_partner definition

-- Drop table

-- DROP TABLE flooring.billing_document_partner;

CREATE TABLE flooring.billing_document_partner (
	billing_document varchar NOT NULL,
	partner_function varchar NOT NULL,
	customer varchar NULL,
	supplier varchar NULL,
	personnel varchar NULL,
	contact_person varchar NULL,
	CONSTRAINT billing_document_partner_pkey PRIMARY KEY (billing_document, partner_function)
);


-- flooring.billing_document_uuid definition

-- Drop table

-- DROP TABLE flooring.billing_document_uuid;

CREATE TABLE flooring.billing_document_uuid (
	billing_document varchar NOT NULL,
	uuid varchar NULL,
	CONSTRAINT billing_document_uuid_pkey PRIMARY KEY (billing_document)
);


-- flooring.bu_pa_address_usage definition

-- Drop table

-- DROP TABLE flooring.bu_pa_address_usage;

CREATE TABLE flooring.bu_pa_address_usage (
	business_partner varchar NOT NULL,
	address_id varchar NOT NULL,
	CONSTRAINT bu_pa_address_usage_pkey PRIMARY KEY (business_partner, address_id)
);


-- flooring.business_partner definition

-- Drop table

-- DROP TABLE flooring.business_partner;

CREATE TABLE flooring.business_partner (
	business_partner varchar NOT NULL,
	customer varchar NULL,
	creation_date timestamptz NOT NULL,
	creation_time varchar NOT NULL,
	last_change_date timestamptz NULL,
	last_change_time varchar NULL,
	organization_bp_name_1 varchar NULL,
	is_marked_for_archiving bool NOT NULL,
	search_term_1 varchar NULL,
	yy1_ax_customer_code_bus varchar NULL,
	CONSTRAINT business_partner_pkey PRIMARY KEY (business_partner)
);


-- flooring.business_partner_address definition

-- Drop table

-- DROP TABLE flooring.business_partner_address;

CREATE TABLE flooring.business_partner_address (
	address_id varchar NOT NULL,
	business_partner varchar NOT NULL,
	house_number varchar NULL,
	street_name varchar NULL,
	city_name varchar NULL,
	region varchar NULL,
	country varchar NULL,
	postal_code varchar NULL,
	CONSTRAINT business_partner_address_pkey PRIMARY KEY (address_id, business_partner)
);


-- flooring.company_code definition

-- Drop table

-- DROP TABLE flooring.company_code;

CREATE TABLE flooring.company_code (
	company_code varchar NOT NULL,
	company_code_name varchar NULL,
	country varchar NULL,
	city_name varchar NULL,
	CONSTRAINT company_code_pkey PRIMARY KEY (company_code)
);


-- flooring.company_code_plant definition

-- Drop table

-- DROP TABLE flooring.company_code_plant;

CREATE TABLE flooring.company_code_plant (
	company_code varchar NOT NULL,
	country_code varchar NULL,
	plant_code varchar NOT NULL,
	plant_description varchar NULL,
	profit_centre varchar NULL,
	CONSTRAINT company_code_plant_pkey PRIMARY KEY (company_code, plant_code)
);


-- flooring.cust_sales_partner_func definition

-- Drop table

-- DROP TABLE flooring.cust_sales_partner_func;

CREATE TABLE flooring.cust_sales_partner_func (
	customer varchar NOT NULL,
	sales_organization varchar NOT NULL,
	distribution_channel varchar NOT NULL,
	division varchar NOT NULL,
	partner_counter varchar NOT NULL,
	partner_function varchar NOT NULL,
	bp_customer_number varchar NULL,
	CONSTRAINT cust_sales_partner_func_pkey PRIMARY KEY (customer, sales_organization, distribution_channel, division, partner_counter, partner_function)
);


-- flooring.customer_company definition

-- Drop table

-- DROP TABLE flooring.customer_company;

CREATE TABLE flooring.customer_company (
	customer varchar NOT NULL,
	company_code varchar NOT NULL,
	CONSTRAINT customer_company_pkey PRIMARY KEY (customer, company_code)
);


-- flooring.customer_sales_area definition

-- Drop table

-- DROP TABLE flooring.customer_sales_area;

CREATE TABLE flooring.customer_sales_area (
	customer varchar NOT NULL,
	sales_organization varchar NOT NULL,
	distribution_channel varchar NOT NULL,
	customer_group varchar NULL,
	customer_price_group varchar NULL,
	division varchar NOT NULL,
	additional_customer_group_1 varchar NULL,
	additional_customer_group_2 varchar NULL,
	CONSTRAINT customer_sales_area_pkey PRIMARY KEY (customer, sales_organization, distribution_channel, division)
);


-- flooring.distribution_channel_text definition

-- Drop table

-- DROP TABLE flooring.distribution_channel_text;

CREATE TABLE flooring.distribution_channel_text (
	distribution_channel varchar NOT NULL,
	distribution_channel_name varchar NOT NULL,
	CONSTRAINT distribution_channel_pk PRIMARY KEY (distribution_channel)
);


-- flooring.employee definition

-- Drop table

-- DROP TABLE flooring.employee;

CREATE TABLE flooring.employee (
	personnel_number varchar NOT NULL,
	uuid varchar NOT NULL,
	CONSTRAINT employee_pkey PRIMARY KEY (personnel_number)
);


-- flooring.matl_stk_in_acct_mod definition

-- Drop table

-- DROP TABLE flooring.matl_stk_in_acct_mod;

CREATE TABLE flooring.matl_stk_in_acct_mod (
	material varchar NOT NULL,
	plant varchar NOT NULL,
	storage_location varchar NOT NULL,
	batch varchar NOT NULL,
	supplier varchar NOT NULL,
	customer varchar NOT NULL,
	wbs_element_internal_id varchar NOT NULL,
	sd_document varchar NOT NULL,
	sd_document_item varchar NOT NULL,
	inventory_special_stock_type varchar NOT NULL,
	inventory_stock_type varchar NOT NULL,
	material_base_unit varchar NULL,
	matl_wrhs_stk_qty_in_matl_base_unit numeric(31, 14) NULL,
	CONSTRAINT matl_stk_in_acct_mod_pkey PRIMARY KEY (material, plant, storage_location, batch, supplier, customer, wbs_element_internal_id, sd_document, sd_document_item, inventory_special_stock_type, inventory_stock_type)
);


-- flooring.outb_delivery_header definition

-- Drop table

-- DROP TABLE flooring.outb_delivery_header;

CREATE TABLE flooring.outb_delivery_header (
	delivery_document varchar NOT NULL,
	delivery_date timestamptz NULL,
	delivery_time varchar NULL,
	actual_goods_movement_date timestamptz NULL,
	actual_goods_movement_time varchar NULL,
	CONSTRAINT outb_delivery_header_pkey PRIMARY KEY (delivery_document)
);


-- flooring.outb_delivery_item definition

-- Drop table

-- DROP TABLE flooring.outb_delivery_item;

CREATE TABLE flooring.outb_delivery_item (
	delivery_document varchar NOT NULL,
	delivery_document_item varchar NOT NULL,
	actual_delivery_quantity varchar NULL,
	reference_sd_document varchar NULL,
	reference_sd_document_item varchar NULL,
	CONSTRAINT outb_delivery_item_pkey PRIMARY KEY (delivery_document, delivery_document_item)
);


-- flooring.product definition

-- Drop table

-- DROP TABLE flooring.product;

CREATE TABLE flooring.product (
	product varchar NOT NULL,
	created_by_user varchar NOT NULL,
	cross_plant_status varchar NULL,
	is_marked_for_deletion varchar NOT NULL,
	last_change_date_time timestamptz NOT NULL,
	product_group varchar NULL,
	product_type varchar NOT NULL,
	creation_date timestamptz NOT NULL,
	last_changed_by_user varchar NULL,
	product_old_id varchar NULL,
	CONSTRAINT product_pkey PRIMARY KEY (product)
);


-- flooring.product_description definition

-- Drop table

-- DROP TABLE flooring.product_description;

CREATE TABLE flooring.product_description (
	product varchar NOT NULL,
	product_description varchar NOT NULL,
	CONSTRAINT product_description_pkey PRIMARY KEY (product)
);


-- flooring.product_group_text definition

-- Drop table

-- DROP TABLE flooring.product_group_text;

CREATE TABLE flooring.product_group_text (
	material_group varchar NOT NULL,
	material_group_text varchar NOT NULL,
	CONSTRAINT product_group_text_pkey PRIMARY KEY (material_group)
);


-- flooring.product_plant definition

-- Drop table

-- DROP TABLE flooring.product_plant;

CREATE TABLE flooring.product_plant (
	plant varchar NOT NULL,
	product varchar NOT NULL,
	profit_center varchar NULL,
	CONSTRAINT product_plant_pkey PRIMARY KEY (plant, product)
);


-- flooring.product_type definition

-- Drop table

-- DROP TABLE flooring.product_type;

CREATE TABLE flooring.product_type (
	product_type varchar NOT NULL,
	product_type_description varchar NOT NULL
);


-- flooring.product_valuation definition

-- Drop table

-- DROP TABLE flooring.product_valuation;

CREATE TABLE flooring.product_valuation (
	product varchar NOT NULL,
	valuation_area varchar NOT NULL,
	valuation_type varchar NOT NULL,
	standard_price numeric(12, 3) NULL,
	CONSTRAINT product_valuation_pkey PRIMARY KEY (product, valuation_area, valuation_type)
);


-- flooring.profit_center definition

-- Drop table

-- DROP TABLE flooring.profit_center;

CREATE TABLE flooring.profit_center (
	profit_center varchar NOT NULL,
	address_name varchar NULL,
	profit_center_standard_hierarchy varchar NULL,
	profit_center_long_name varchar NULL,
	CONSTRAINT profit_center_pkey PRIMARY KEY (profit_center)
);


-- flooring.profit_centre_group_mapping definition

-- Drop table

-- DROP TABLE flooring.profit_centre_group_mapping;

CREATE TABLE flooring.profit_centre_group_mapping (
	profit_centre_group varchar NOT NULL,
	profit_centre_group_name varchar NOT NULL,
	CONSTRAINT profit_centre_group_mapping_pkey PRIMARY KEY (profit_centre_group)
);


-- flooring.sales_order definition

-- Drop table

-- DROP TABLE flooring.sales_order;

CREATE TABLE flooring.sales_order (
	sales_order varchar NOT NULL,
	creation_date timestamptz NULL,
	last_change_date_time timestamptz NULL,
	sold_to_party varchar NULL,
	total_net_amount varchar NULL,
	transaction_currency varchar NULL,
	purchase_order_by_customer varchar NULL,
	requested_delivery_date timestamptz NULL,
	sales_order_date timestamptz NULL,
	sales_organization varchar NULL,
	created_by_user varchar NULL,
	distribution_channel varchar NULL,
	delivery_block_reason varchar NULL,
	overall_rejection_status varchar NULL,
	overall_process_status varchar NULL,
	CONSTRAINT sales_order_pkey PRIMARY KEY (sales_order)
);


-- flooring.sales_order_core definition

-- Drop table

-- DROP TABLE flooring.sales_order_core;

CREATE TABLE flooring.sales_order_core (
	core_id varchar NOT NULL,
	sales_order varchar NOT NULL,
	CONSTRAINT sales_order_core_pkey PRIMARY KEY (core_id),
	CONSTRAINT sales_order_core_sales_order_key UNIQUE (sales_order)
);


-- flooring.sales_order_header_partner definition

-- Drop table

-- DROP TABLE flooring.sales_order_header_partner;

CREATE TABLE flooring.sales_order_header_partner (
	sales_order varchar NOT NULL,
	partner_function varchar NOT NULL,
	customer varchar NULL,
	CONSTRAINT sales_order_header_partner_pkey PRIMARY KEY (sales_order, partner_function)
);


-- flooring.sales_order_item definition

-- Drop table

-- DROP TABLE flooring.sales_order_item;

CREATE TABLE flooring.sales_order_item (
	sales_order varchar NOT NULL,
	sales_order_item varchar NOT NULL,
	material varchar NULL,
	net_amount numeric(16, 3) NULL,
	requested_quantity_unit varchar NULL,
	transaction_currency varchar NULL,
	production_plant varchar NULL,
	rejection_reason varchar NULL,
	CONSTRAINT sales_order_item_pkey PRIMARY KEY (sales_order, sales_order_item)
);


-- flooring.sales_order_item_core definition

-- Drop table

-- DROP TABLE flooring.sales_order_item_core;

CREATE TABLE flooring.sales_order_item_core (
	core_id varchar NOT NULL,
	sales_order varchar NOT NULL,
	sales_order_item varchar NOT NULL,
	CONSTRAINT sales_order_item_core_pkey PRIMARY KEY (core_id),
	CONSTRAINT sales_order_item_core_sales_order_sales_order_item_key UNIQUE (sales_order, sales_order_item)
);


-- flooring.sales_order_item_pr_element definition

-- Drop table

-- DROP TABLE flooring.sales_order_item_pr_element;

CREATE TABLE flooring.sales_order_item_pr_element (
	sales_order varchar NOT NULL,
	sales_order_item varchar NOT NULL,
	pricing_procedure_step varchar NOT NULL,
	pricing_procedure_counter varchar NOT NULL,
	condition_type varchar NULL,
	condition_amount numeric(16, 3) NULL,
	condition_rate_value numeric(24, 9) NULL,
	condition_inactive_reason varchar NULL,
	condition_class varchar NULL,
	condition_category varchar NULL,
	condition_calculation_type varchar NULL,
	CONSTRAINT sales_order_item_pr_element_pkey PRIMARY KEY (sales_order, sales_order_item, pricing_procedure_step, pricing_procedure_counter)
);


-- flooring.sales_order_schedule_line definition

-- Drop table

-- DROP TABLE flooring.sales_order_schedule_line;

CREATE TABLE flooring.sales_order_schedule_line (
	sales_order varchar NOT NULL,
	sales_order_item varchar NOT NULL,
	schedule_line varchar NOT NULL,
	schedule_line_order_quantity varchar NULL,
	confirmed_delivery_date timestamptz NULL,
	requested_delivery_date timestamptz NULL,
	CONSTRAINT schedule_line_pkey PRIMARY KEY (sales_order, sales_order_item, schedule_line)
);


-- flooring.sales_order_uuid definition

-- Drop table

-- DROP TABLE flooring.sales_order_uuid;

CREATE TABLE flooring.sales_order_uuid (
	sales_order varchar NOT NULL,
	uuid varchar NULL,
	CONSTRAINT sales_order_uuid_pkey PRIMARY KEY (sales_order)
);


-- flooring.sales_organization definition

-- Drop table

-- DROP TABLE flooring.sales_organization;

CREATE TABLE flooring.sales_organization (
	sales_organization varchar NOT NULL,
	company_code varchar NULL,
	CONSTRAINT sales_organization_pkey PRIMARY KEY (sales_organization)
);


-- flooring.sales_pricing_condition_record definition

-- Drop table

-- DROP TABLE flooring.sales_pricing_condition_record;

CREATE TABLE flooring.sales_pricing_condition_record (
	condition_record varchar NOT NULL,
	condition_sequential_number varchar NOT NULL,
	condition_table varchar NOT NULL,
	condition_validity_start_date timestamptz NOT NULL,
	condition_validity_end_date timestamptz NULL,
	condition_application varchar NOT NULL,
	condition_type varchar NOT NULL,
	condition_release_status varchar NULL,
	created_by_user varchar NULL,
	creation_date timestamptz NOT NULL,
	pricing_scale_type varchar NULL,
	pricing_scale_basis varchar NULL,
	condition_scale_quantity varchar NULL,
	condition_scale_quantity_unit varchar NULL,
	condition_scale_amount varchar NULL,
	condition_scale_amount_currency varchar NULL,
	condition_calculation_type varchar NULL,
	condition_rate_value numeric(16, 3) NULL,
	condition_rate_value_unit varchar NULL,
	condition_quantity varchar NULL,
	condition_quantity_unit varchar NULL,
	condition_is_deleted bool NULL
);


-- flooring.sales_pricing_condition_record_scale definition

-- Drop table

-- DROP TABLE flooring.sales_pricing_condition_record_scale;

CREATE TABLE flooring.sales_pricing_condition_record_scale (
	condition_record varchar NOT NULL,
	condition_sequential_number varchar NOT NULL,
	condition_scale_line varchar NOT NULL,
	condition_scale_quantity varchar NULL,
	condition_scale_quantity_unit varchar NULL,
	condition_scale_amount numeric(16, 3) NULL,
	condition_scale_amount_currency varchar NULL,
	condition_rate_value numeric(16, 3) NULL,
	condition_rate_value_unit varchar NULL
);


-- flooring.sales_pricing_condition_record_validity definition

-- Drop table

-- DROP TABLE flooring.sales_pricing_condition_record_validity;

CREATE TABLE flooring.sales_pricing_condition_record_validity (
	condition_record varchar NOT NULL,
	condition_validity_start_date timestamptz NOT NULL,
	condition_validity_end_date timestamptz NULL,
	condition_application varchar NOT NULL,
	condition_type varchar NOT NULL,
	condition_release_status varchar NULL,
	sales_organization varchar NULL,
	division varchar NULL,
	distribution_channel varchar NULL,
	material varchar NULL,
	material_group varchar NULL,
	material_pricing_group varchar NULL,
	customer varchar NULL,
	customer_group varchar NULL,
	customer_price_group varchar NULL
);


-- flooring.source_system definition

-- Drop table

-- DROP TABLE flooring.source_system;

CREATE TABLE flooring.source_system (
	source_system_name varchar NOT NULL,
	uuid varchar NOT NULL,
	CONSTRAINT source_system_pkey PRIMARY KEY (source_system_name)
);


-- flooring.yy1_add_cust_group_text_01 definition

-- Drop table

-- DROP TABLE flooring.yy1_add_cust_group_text_01;

CREATE TABLE flooring.yy1_add_cust_group_text_01 (
	additional_customer_group_1 varchar NOT NULL,
	additional_customer_group_1_name varchar NULL,
	CONSTRAINT yy1_add_cust_group_text_01_pkey PRIMARY KEY (additional_customer_group_1)
);


-- flooring.yy1_add_cust_group_text_02 definition

-- Drop table

-- DROP TABLE flooring.yy1_add_cust_group_text_02;

CREATE TABLE flooring.yy1_add_cust_group_text_02 (
	additional_customer_group_2 varchar NOT NULL,
	additional_customer_group_2_name varchar NULL,
	CONSTRAINT yy1_add_cust_group_text_02_pkey PRIMARY KEY (additional_customer_group_2)
);