# SQL DDLs for Postgres Landing tables
These are generated DDLs for the table defintions for the TCG transform landing tables.

# flooring.sql
This schema and the tables are used by the flooring project.

# public.sql
This schema and the tables within are used for the finance project.

# cart.sql
This schema and the tables within are used for mapping BigCommerce Customer and
Product IDs to SAP IDs. Also used for identifying sold to and ship to
relationships.

Note used in the flooring project.

# pricing.sql
This schema and the tables are used for generating list pricing for products in
BigCommerce.

Note used in the flooring project.

# bc.sql

No longer used.
